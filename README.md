# go

Statically typed, compiled. https://golang.org

* [*Go (programming language)*
  ](https://en.m.wikipedia.org/wiki/Go_(programming_language))

# Transpiler
* [Borgo](https://borgo-lang.github.io)
  * [borgo-lang/borgo](https://github.com/borgo-lang/borgo)

# Functional programming
* *Functional programming in Go : apply functional techniques in Golang to improve the testability, readability, and security of your code*
  2023 Dylan Meeus
* [*7 Easy functional programming techniques in Go*
  ](https://deepu.tech/functional-programming-in-go/)
  2019-08 Deepu K Sasidharan
* [*Learning Functional Programming in Go*
  ](https://www.worldcat.org/search?q=ti%3ALearning+Functional+Programming+in+Go)
  2017 Lex Sheehan

# Libraries
## ORM
* [gorm](https://pkg.go.dev/gorm.io/gorm)

## Web
* [gin](https://pkg.go.dev/github.com/gin-gonic/gin)
